import axios from 'axios';

const loginUserApi = (email, phone , password) => {
    return axios.post(`http://localhost:4000/api/user/login`, { email, phone, password })
        .then((result) => result)
        .catch((error) => console.log(error))
}
const registerUserApi = (firstName, lastName, email, phone, password) => {
    return axios.post(`http://localhost:4000/api/user/add`, { firstName, lastName, email, phone, password })
        .then((result) => result)
        .catch((error) => console.log(error))
}
const userId = (userid) => {
    return axios.get(`http://localhost:4000/api/user/list?id=${userid}`)
        .then((result) => console.log(result))
        .catch((error) => console.log(error))
}
const forgetUserPassword = (email , phone) => {
    return axios.post(`http://localhost:4000/api/user/forgetpassword`,{email})
        .then((result) => result)
        .catch((error) => console.log(error))
}
const resetUserPassword = (uniqueKey,New_password, confirm_New_password) => {
    // console.log(uniqueKey)
    return axios.post(`http://localhost:4000/api/user/resetpassword?uniqueKey=${uniqueKey}`,{New_password , confirm_New_password})
    .then((result) => result)
    .catch((error) => console.log(error))
}
const editaccount = (id,firstName,lastName,email,phone) => {
    const Id = JSON.parse(localStorage.getItem('gogo_current_user'))
    return axios.post(`http://localhost:4000/api/user/edit/${Id.id}`,{firstName,lastName,email,phone})
    .then((result)=>result)
    .catch((error) =>error)
}
export {
    loginUserApi,
    registerUserApi,
    userId,
    forgetUserPassword,
    resetUserPassword,
    editaccount,
}
