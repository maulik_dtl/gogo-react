import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import { auth } from '../../helpers/Firebase';
import { loginUserApi, registerUserApi,userId, editaccount, forgetUserPassword, resetUserPassword } from '../../helpers/api';
import {
  LOGIN_USER,
  REGISTER_USER,
  AUTHORNAME,
  EDITUSER,
  ACCOUNTEDIT,
  ACCOUNTEDIT_SUCCESS,
  ACCOUNTEDIT_ERROR,
  LOGOUT_USER,
  FORGOT_PASSWORD,
  RESET_PASSWORD,
} from '../actions';

import {
  loginUserSuccess,
  loginUserError,
  authorName,
  editUser,
  accountEdit,
  editAccount,
  accountEditSuccess,
  accountEditError,
  registerUserSuccess,
  registerUserError,
  forgotPasswordSuccess,
  forgotPasswordError,
  resetPasswordSuccess,
  resetPasswordError,
  logoutUser,
} from './actions';

import { adminRoot, currentUser } from '../../constants/defaultValues';
import { setCurrentUser } from '../../helpers/Utils';
import { reset } from 'mousetrap';

// login User 
export function* watchLoginUser() {
  // eslint-disable-next-line no-use-before-define
  yield takeEvery(LOGIN_USER, loginWithEmailPassword);
}
function* loginWithEmailPassword({ payload }) {
  const { email, phone, password } = payload.user;
 
  const { history } = payload;
  try {
    const loginUser = yield call(loginUserApi,  email, phone, password);

    if (loginUser.data.status==true) {
      // console.log(loginUser.data.data[0].id)
      const item = { ...currentUser,
                    id : loginUser.data.data[0].id,
                    firstName : loginUser.data.data[0].firstName,
                    lastName : loginUser.data.data[0].lastName,
                    email : loginUser.data.data[0].email,
                    phone : loginUser.data.data[0].phone,
                    profilePicture : loginUser.data.data[0].profilePicture
                  };
      const userName ={firstName:loginUser.data.data[0].firstName , lastName:loginUser.data.data[0].lastName}
      setCurrentUser(item);
      yield put(loginUserSuccess(item));
      yield put(authorName(userName))
      console.log(localStorage.gogo_current_user)

      var current =JSON.parse( localStorage.getItem('gogo_current_user'))
       console.log('gogo_current_user:' , current.firstName)
     
     // var ud = JSON.parse(localStorage.getItem(firstName))
     // console.log(ud)
      history.push(adminRoot);
    } else {
      yield put(loginUserError(loginUser.message));
    }
  } catch (error) {
    yield put(loginUserError(error));
  }
}


export function* watchRegisterUser() {
  // console.log(takeEvery(REGISTER_USER, registerWithEmailPassword));
  yield takeEvery(REGISTER_USER, registerWithEmailPassword);
}
function* registerWithEmailPassword({ payload }) {
 
  const { firstName, lastName, email, phone, password  } = payload.user;
  const { history } = payload;
  try {
    const registerUser = yield call(
      registerUserApi,
      firstName, lastName, email, phone, password
    );
    // history.push('/user/register');
    if (registerUser.data.status==true) {
      const item = { uid: registerUser.data.email, ...currentUser };
      setCurrentUser(item);
      yield put(registerUserSuccess(item));
      history.push(adminRoot);
    } else {
      yield put(registerUserError(registerUser.message));
    }
  } catch (error) {
    yield put(registerUserError(error));
  }
}


// to get author name
export function* watchAuthor() {
  console.log("watchauthor 1");
  // console.log(takeEvery(REGISTER_USER, registerWithEmailPassword));
  yield takeEvery(AUTHORNAME, toGetAuthor);
}
function* toGetAuthor({ payload }) {
  // console.log("watchauthor 22222");

 
  // const { firstName, lastName, email, phone, password  } = payload.user;
  const { history } = payload;
  try {
    const Username = yield call(userId , id );
    // history.push('/user/register');
    if (Username.data.status==true) {
      const item = { uid: Username.data.id, ...currentUser };
      setCurrentUser(item);
      yield put(authorName(item));
      history.push(adminRoot);
    } else {
      yield put(registerUserError(registerUser.message));
    }
  } catch (error) {
    yield put(registerUserError(error));
  }
}

//edit profile
export function* watchEditUserProfile(){
  // console.log("edit user saga")
  yield takeEvery(EDITUSER ,editUserDetails);
}
function* editUserDetails({payload}){
  // console.log(payload);
  // const {firstName, lastName, email, phone} = payload.edituser;
  const userid=payload.edituser
  try{
    const editProfile = yield call(userId, userid, firstName, lastName, email, phone);
    if(editProfile.data.status == true){
      
      console.log(editUserDetails.data)
      yield put(editUser(editProfile));

    }
  }catch(errro){
    yield put(error);
  }
}

// Edit Account 
export function* watchEditAccount() {
  // console.log(takeEvery(REGISTER_USER, registerWithEmailPassword));
  yield takeEvery(ACCOUNTEDIT, accountEditDetails);
}

function* accountEditDetails({payload}){ 
  // console.log("edit user")
  // console.log(payload);
  const {firstName,lastName,email,phone,profilePicture} = payload;
  const Id = payload.accountEdit
  try{
    const editUserProfile = yield call(editaccount,Id,firstName, lastName, email, phone,profilePicture);
    console.log(editUserProfile);
    if(editUserProfile.data.status == true){
      console.log(profilePicture)
      // setCurrentUser();
 
      const item = { ...currentUser,
                      id : JSON.parse(localStorage.getItem('gogo_current_user')).id,
                      firstName : firstName,
                      lastName : lastName,
                      email : email,
                      phone : phone,
                      profilePicture : typeof profilePicture != 'undefined'?profilePicture:JSON.parse(localStorage.getItem('gogo_current_user')).profilePicture,
      };
      // console.log(profilePicture)
      setCurrentUser(item);
      // console.log(localStorage.getItem('gogo_current_user'))

      yield put(accountEditSuccess(item));
      // console.log("Account edit saga")
    }else{
      console.log(",,,,,,,,,")
      yield put(accountEditError(editUserProfile.message));
    }
  }catch(error){
    console.log(error)

    yield put(accountEditError(error));
  }
}



export function* watchLogoutUser() {
  // eslint-disable-next-line no-use-before-define
  yield takeEvery(LOGOUT_USER, logout);
}

const logoutAsync = async (history) => {
  await auth
    .signOut()
    .then((user) => user)
    .catch((error) => error);
  history.push(adminRoot);
};

function* logout({ payload }) {
  const { history } = payload;
  setCurrentUser();
  yield call(logoutAsync, history);
}

export function* watchForgotPassword() {
  // eslint-disable-next-line no-use-before-define
  yield takeEvery(FORGOT_PASSWORD, forgotPassword);
}
// const forgotPasswordAsync = async (email) => {
//   // eslint-disable-next-line no-return-await
//   return await auth
//     .sendPasswordResetEmail(email)
//     .then((user) => user)
//     .catch((error) => error);
// };

//Forgot Password 
function* forgotPassword({ payload }) {
  const { email } = payload.forgotUserMail;
  
  try {
    const forgotPasswordStatus = yield call(forgetUserPassword, email);
    if (forgotPasswordStatus.data.status == true) {
      console.log(forgotPasswordStatus)

      yield put(forgotPasswordSuccess('success'));
     
    } else {
      yield put(forgotPasswordError(forgotPasswordStatus.data.message));
    }
  } catch (error) {
    yield put(forgotPasswordError(error));
  }
}

export function* watchResetPassword() {
  // eslint-disable-next-line no-use-before-define
  yield takeEvery(RESET_PASSWORD, resetPassword);
}

function* resetPassword({ payload }) {
  const { New_password, confirm_New_password } = payload;
  // console.log("reset apiiiii");
  try {
      const uniqeyKey = new URLSearchParams(location.search)
    const resetPasswordStatus = yield call(resetUserPassword,uniqeyKey.get('uniqueKey'), New_password, confirm_New_password );
    // console.log("reset 1");
    if (resetPasswordStatus.data.status == true) {
      // console.log(resetPasswordStatus);
      yield put(resetPasswordSuccess('success'));
    } else {
      yield put(resetPasswordError(resetPasswordStatus.data.message));
    }
  } catch (error) {
    yield put(resetPasswordError(error));
  }
}

export default function* rootSaga() {
  yield all([
    fork(watchLoginUser),
    fork(watchAuthor),
    fork(watchLogoutUser),
    fork(watchRegisterUser),
    fork(watchForgotPassword),
    fork(watchResetPassword),
    fork(watchEditUserProfile),
    fork(watchEditAccount),
  ]);
}
