import {
  LOGIN_USER,
  LOGIN_USER_SUCCESS,
  LOGOUT_USER,
  REGISTER_USER,
  AUTHORNAME,
  EDITUSER,
  ACCOUNTEDIT,
  ACCOUNTEDIT_SUCCESS,
  ACCOUNTEDIT_ERROR,
  REGISTER_USER_SUCCESS,
  LOGIN_USER_ERROR,
  REGISTER_USER_ERROR,
  FORGOT_PASSWORD,
  FORGOT_PASSWORD_SUCCESS,
  FORGOT_PASSWORD_ERROR,
  RESET_PASSWORD,
  RESET_PASSWORD_SUCCESS,
  RESET_PASSWORD_ERROR,
} from '../actions';

export const loginUser = (user, history) => ({
  type: LOGIN_USER,
  payload: { user, history },
});
export const loginUserSuccess = (user) => ({
  type: LOGIN_USER_SUCCESS,
  payload: user,
});
export const loginUserError = (message) => ({
  type: LOGIN_USER_ERROR,
  payload: { message },
});
export const authorName = (authorname) => ({
  type: AUTHORNAME,
  payload: { authorname },
});

export const editUser = (edituser) => ({
  type : EDITUSER,
  payload:{edituser},
});

//edit Account POST
export const accountEdit = (EditUser) => (console.log(EditUser),{
  type : ACCOUNTEDIT,
  payload:EditUser,
})
export const accountEditSuccess = (userDetails) => ({
  type : ACCOUNTEDIT_SUCCESS,
  payload:{userDetails},
});
export const accountEditError = (message) => ({
  type:ACCOUNTEDIT_ERROR,
  payload:{message},
})

//forgotPassword
export const forgotPassword = (forgotUserMail, history) => ({
  type: FORGOT_PASSWORD,
  payload: { forgotUserMail, history },
});
export const forgotPasswordSuccess = (forgotUserMail) => ({
  type: FORGOT_PASSWORD_SUCCESS,
  payload: forgotUserMail,
});
export const forgotPasswordError = (message) => ({
  type: FORGOT_PASSWORD_ERROR,
  payload: { message },
});

export const resetPassword = ({ New_password, confirm_New_password,newpassword, history }) => ({
  type: RESET_PASSWORD,
  payload: { New_password, confirm_New_password,newpassword, history },
});
export const resetPasswordSuccess = (newpassword) => ({
  type: RESET_PASSWORD_SUCCESS,
  payload: newpassword,
});
export const resetPasswordError = (message) => ({
  type: RESET_PASSWORD_ERROR,
  payload: { message },
});

export const registerUser = (user, history) => ({
  type: REGISTER_USER,
  payload: { user, history },
});
export const registerUserSuccess = (user) => ({
  type: REGISTER_USER_SUCCESS,
  payload: user,
});
export const registerUserError = (message) => ({
  type: REGISTER_USER_ERROR,
  payload: { message },
});


export const logoutUser = (history) => ({
  type: LOGOUT_USER,
  payload: { history },
});
