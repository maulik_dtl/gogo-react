import React, { useState, useEffect } from 'react';

import { injectIntl } from 'react-intl';
import { connect } from 'react-redux';

import {
  CustomInput,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Input,
  Label,
} from 'reactstrap';
import Select from 'react-select';
import CustomSelectInput from '../../components/common/CustomSelectInput';
import IntlMessages from '../../helpers/IntlMessages';
import { accountEdit } from '../../redux/actions';
import { setCurrentUser } from '../../helpers/Utils';
import { NotificationManager } from '../../components/common/react-notifications';



const AddNewModal = ({ modalOpen=true, toggleModal,loading,error, categories,editUser,userinfo ,accountEdit}) => {
  const [firstName,setFirstName] = useState(userinfo.edituser.firstName);
  const [lastName , setLastName] = useState(userinfo.edituser.lastName);
  const [email, setEmail] = useState(userinfo.edituser.email);
  const [phone ,setPhone] = useState(userinfo.edituser.phone);

  
  const editAccount=()=>{
    
    var editAccountData = {firstName,lastName,email,phone}
    accountEdit(editAccountData);
    

    // setCurrentUser(editAccountData);
  //  toggleModal()
  }
// console.log(userinfo);
var current =JSON.parse( localStorage.getItem('gogo_current_user'))
  return (
    <>
    <Modal
      isOpen={modalOpen}
      toggle={toggleModal}
      wrapClassName="modal-right"
      backdrop="static"
    >
  
      <ModalHeader toggle={toggleModal}>
        <IntlMessages id="Edit Account Details" />
      </ModalHeader>
      <ModalBody>
        <div className="text-center">
        <img src={"http://localhost:4000/images/"+current.profilePicture} alt="Card image cap" class="img-thumbnail border-0 rounded-circle mb-4 list-thumbnail card-img-top"/>
        </div>
        <div className="text-center">
        <Button className="btn-sm" color="secondary" outline>Change
        </Button>
        </div>

        
        <Label className="">
          <IntlMessages id="first-Name" />
        </Label>
        <Input name={"firstName"} type="text"  onChange={(e)=>{setFirstName(e.target.value)}} value={firstName}/>

        <Label className="mt-3">
          <IntlMessages id="last-Name" />
        </Label>
        <Input name={"lastName"} type="text"  onChange={(e)=>{setLastName(e.target.value)}} value={lastName}/>
        
        <Label className="mt-3">
          <IntlMessages id="email" />
        </Label>
        <Input name={"email"} type="email"  onChange={(e)=>{setEmail(e.target.value)}} value={email}/>
        
        <Label className="mt-3">
          <IntlMessages id="phone" />
        </Label>
        <Input name={"phone"} type="number"  onChange={(e)=>{setPhone(e.target.value)}} value={phone}/>
        
         
      </ModalBody>
      <ModalFooter>
        <Button color="secondary" outline onClick={toggleModal}>
          <IntlMessages id="Cancel"/>
        </Button>
        <Button color="primary" onClick={(event,error,value)=>editAccount(event,error,value)}>
          <IntlMessages id="Submit" />
        </Button>{' '}
      </ModalFooter>
    </Modal>
    </>
  );
};

const mapStateToProps = ({authUser, menu, settings }) => {
  const {loading,error} = authUser;
  return {
    loading,
    error,

  };
};
export default 
(
  connect(mapStateToProps, {
    accountEdit:accountEdit
  })(AddNewModal)
);

