import React, { useState } from 'react';
import {
  Row,
  Card,
  CardTitle,
  Form,
  FormGroup,
  Label,
  Input,
  Button,
} from 'reactstrap';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { registerUser } from '../../redux/actions';

import IntlMessages from '../../helpers/IntlMessages';
import { Colxx } from '../../components/common/CustomBootstrap';
import { adminRoot } from '../../constants/defaultValues';

const Register = ({ history, registerUserAction}) => {
  const [firstName, setfirstName] = useState('');
  const [lastName, setlastName] = useState('');
  const [email, setemail] = useState('');
  const [phone, setphone] = useState('');
  const [password, setpassword] = useState('');

  const onUserRegister = () => {
    // console.log("submitttttttttttttttt");
      const user={
        firstName:firstName,
        lastName:lastName,
        email:email,
        phone:phone,
        password:password}
        // console.log(user);
    if (firstName !== '' && lastName !== '' && email !== '' && phone !== '' && password !== '') {
      // console.log("submit succse")
      registerUserAction(user,history)
      // history.push(adminRoot);
    }
    // call registerUserAction()
  };
  const initialValues ={firstName,lastName,email,phone,password}
  return (
    <Row className="h-100">
      <Colxx xxs="12" md="10" className="mx-auto my-auto">
        <Card className="auth-card">
          <div className="position-relative image-side ">
            <p className="text-white h2">MAGIC IS IN THE DETAILS</p>
            <p className="white mb-0">
              Please use this form to register. <br />
              If you are a member, please{' '}
              <NavLink to="/user/login" className="white">
                login
              </NavLink>
              .
            </p>
          </div>
          <div className="form-side">
            <NavLink to="/" className="white">
              <span className="logo-single" />
            </NavLink>
            <CardTitle className="mb-4">
              <IntlMessages id="user.register" />
            </CardTitle>
            <Form>
              <FormGroup className="form-group has-float-label  mb-4">
                <Label>
                  <IntlMessages id="Firs tName" />
                </Label>
                <Input type="name" onChange={(e)=>{setfirstName(e.target.value)}} defaultValue={firstName} />
              </FormGroup>

              <FormGroup className="form-group has-float-label  mb-4">
                <Label>
                  <IntlMessages id="Last Name" />
                </Label>
                <Input type="name" onChange={(e)=>{setlastName(e.target.value)}} defaultValue={lastName} />
              </FormGroup>

              <FormGroup className="form-group has-float-label  mb-4">
                <Label>
                  <IntlMessages id="Email" />
                </Label>
                <Input type="email" onChange={(e)=>{setemail(e.target.value)}} defaultValue={email} />
              </FormGroup>

              <FormGroup className="form-group has-float-label  mb-4">
                <Label>
                  <IntlMessages id="phone" />
                </Label>
                <Input type="number" onChange={(e)=>{setphone(e.target.value)}} defaultValue={phone} />
              </FormGroup>

              <FormGroup className="form-group has-float-label  mb-4">
                <Label>
                  <IntlMessages id="Password"  />
                </Label>
                <Input type="password" onChange={(e)=>{setpassword(e.target.value)}} defaultValue={password} />
              </FormGroup>

              <div className="d-flex justify-content-end align-items-center">
                <Button
                  color="primary"
                  className="btn-shadow"
                  size="lg"
                  // initialValues={initialValues}
                  onClick={() => onUserRegister()}
                >
                  <IntlMessages id="user.register-button" />
                </Button>
              </div>
            </Form>
          </div>
        </Card>
      </Colxx>
    </Row>
  );
};
const mapStateToProps = () => {};

export default connect(mapStateToProps, {
  registerUserAction: registerUser,
})(Register);
