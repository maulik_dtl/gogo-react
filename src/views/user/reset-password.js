import React, { useState, useEffect } from 'react';
import { Row, Card, CardTitle, Label, FormGroup, Button } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import { Formik, Form, Field } from 'formik';
import { connect } from 'react-redux';
import { Colxx } from '../../components/common/CustomBootstrap';
import IntlMessages from '../../helpers/IntlMessages';
import { resetPassword } from '../../redux/actions';
import { NotificationManager } from '../../components/common/react-notifications';

const validateNew_password = (values) => {
  const { New_password, confirm_New_password } = values;
  const errors = {};
  if (confirm_New_password && New_password !== confirm_New_password) {
    errors.confirm_New_password = 'Please check your new password';
  }
  return errors;
};

const ResetPassword = ({
  location,
  newpassword,
  history,
  loading,
  error,
  resetPasswordAction,
}) => {
  const [New_password] = useState('');
  const [confirm_New_password] = useState('');

  useEffect(() => {
    if (error) {
      NotificationManager.warning(
        error,
        'Forgot Password Error',
        3000,
        null,
        null,
        ''
      );
    } else if (!loading && newpassword === 'success')
      NotificationManager.success(
        'Please login with your new password.',
        'Reset Password Success',
        3000,
        null,
        null,
        ''
      );
  }, [error, newpassword,  loading]);

  const onResetPassword = (values) => {
    // console.log(values);
    // console.log(location.search);
    if (!loading) {
      // console.log(params)
       
      // console.log(uniqeyKey.get('uniqueKey'))
      // const oobCode = params.get('oobCode');
        if (values.New_password !== '') {
          resetPasswordAction({
            New_password: values.New_password,
            confirm_New_password:values.confirm_New_password ,
            history,
          });
        }
     
    }
  };

  const initialValues = { New_password, confirm_New_password };

  return (
    <Row className="h-100">
      <Colxx xxs="12" md="10" className="mx-auto my-auto">
        <Card className="auth-card">
          <div className="position-relative image-side ">
            <p className="text-white h2">MAGIC IS IN THE DETAILS</p>
            <p className="white mb-0">
              Please use your e-mail to reset your password. <br />
              If you are not a member, please{' '}
              <NavLink to="/register" className="white">
                register
              </NavLink>
              .
            </p>
          </div>
          <div className="form-side">
            <NavLink to="/" className="white">
              <span className="logo-single" />
            </NavLink>
            <CardTitle className="mb-4">
              <IntlMessages id="user.reset-password" />
            </CardTitle>

            <Formik
              validate={validateNew_password}
              initialValues={initialValues}
              onSubmit={onResetPassword}
            >
              {({ errors, touched }) => (
                <Form className="av-tooltip tooltip-label-bottom">
                  <FormGroup className="form-group has-float-label">
                    <Label>
                      <IntlMessages id="user.new-password" />
                    </Label>
                    <Field
                      className="form-control"
                      name="New_password"
                      type="password"
                    />
                  </FormGroup>
                  <FormGroup className="form-group has-float-label">
                    <Label>
                      <IntlMessages id="user.new-password-again" />
                    </Label>
                    <Field
                      className="form-control"
                      name="confirm_New_password"
                      type="password"
                    />
                    {errors.confirm_New_password && touched.confirm_New_password && (
                      <div className="invalid-feedback d-block">
                        {errors.confirm_New_password}
                      </div>
                    )}
                  </FormGroup>

                  <div className="d-flex justify-content-between align-items-center">
                    <NavLink to="/user/login">
                      <IntlMessages id="user.login-title" />
                    </NavLink>
                    <Button
                      color="primary"
                      className={`btn-shadow btn-multiple-state ${
                        loading ? 'show-spinner' : ''
                      }`}
                      size="lg"
                    >
                      <span className="spinner d-inline-block">
                        <span className="bounce1" />
                        <span className="bounce2" />
                        <span className="bounce3" />
                      </span>
                      <span className="label">
                        <IntlMessages id="user.reset-password-button" />
                      </span>
                    </Button>
                  </div>
                </Form>
              )}
            </Formik>
          </div>
        </Card>
      </Colxx>
    </Row>
  );
};

const mapStateToProps = ({ authUser }) => {
  const { New_password, confirm_New_password, loading, newpassword, error } = authUser;
  return { New_password, confirm_New_password, loading, newpassword, error };
};

export default connect(mapStateToProps, {
  resetPasswordAction: resetPassword,
})(ResetPassword);
